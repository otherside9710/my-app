import { Component} from '@angular/core';

@Component({
  selector: "app-table",
  templateUrl: 'table.component.html'
})

export class TableComponent {
  public head_name = 'nombres';
  public head_apellidos = 'apellidos';
  public head_prof = 'profesion';
  public nombres = 'Julio Cesar';
  public apellidos = 'Sarmiento Peña';
  public profesion = 'Ing. Sistemas';
}
