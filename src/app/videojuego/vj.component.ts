import {Component} from '@angular/core';

@Component({
  selector: 'app-videojuego',
  template: `<h3>Esta es mi componente de {{nombre}}</h3>`
})

export class VideoJuegosComponent {
  public nombre = 'Tabla';
}
