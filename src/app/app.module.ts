import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {VideoJuegosComponent} from './videojuego/vj.component';
import {TableComponent} from './table/table.component';
import {NgIfComponent} from './ng-if/ngIf.component';
import {NgForComponent} from './ng-for/ngFor.component';

@NgModule({
  declarations: [
    AppComponent,
    VideoJuegosComponent,
    TableComponent,
    NgIfComponent,
    NgForComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
