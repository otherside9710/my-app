import {Component} from '@angular/core';

@Component({
  selector: 'nf-for',
  templateUrl: 'ngFor.component.html'
})

export class NgForComponent {
  public head_name = 'Nombres';
  public array = ['julio', 'edgar', 'fabian', 'dony'];
}
