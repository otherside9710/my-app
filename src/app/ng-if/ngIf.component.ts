import {Component} from '@angular/core';

@Component({
  selector: 'ngif-if',
  template: `<h2>Componente de {{name_component}} con Styles</h2><br/>
  <h3 *ngIf="flag" [style.background] = colort>{{msgt}}</h3>  
  <h3 *ngIf="!flag" [style.background] = colorf>{{msgf}}</h3>  
  `
})

export class NgIfComponent {
  public name_component = 'NgIf';
  public flag = true;
  public msgt = 'VERDADERO';
  public msgf = 'FALSO';
  public colort = 'green';
  public colorf = 'red';
}
