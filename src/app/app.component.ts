import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'JSP - ANGULAR 5';
  descripcion = 'Hola, esta es mi primera app con angular 5';
}
